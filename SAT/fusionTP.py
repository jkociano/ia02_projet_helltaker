from typing import List, Tuple
import subprocess
import sys
from itertools import combinations

from helltaker_utils import grid_from_file, check_plan

#labySATPLAN
myLaby = {
    "walls": {(2, 2), (2, 3), (4, 3)},
    "board": {(x, y) for x in range(1, 5) for y in range(1, 5) if (x, y) not in {(2, 2), (2, 3), (4, 3)}},
    "initial": (2, 1),
    "final": (1, 4)}

#print(myLaby)

#helltaker
infosGrille=grid_from_file(sys.argv[1])
#print(infosGrille)

myLabyHell = {
    "walls": {},
    "spike": {},
    "board": {},
    "initial":(0,0),
    "final":{}
}

walls = set()
spike = set()
board = set()
final = set()
def grid_to_laby() :
    x = 1
    y = infosGrille["m"]
    for it in infosGrille["grid"]:
        x = 1
        for it2 in it:
            if it2 == "#" : #ecas mur
                walls.add((x,y))

            elif it2 == "H" : #cas Perso
                myLabyHell["initial"] = (x,y)
                board.add((x,y))

            elif it2 == "D" : #cas demon
                final.add((x-1,y))
                final.add((x+1,y))
                final.add((x,y-1))
                final.add((x,y+1))

            elif it2 == " " : #cas blanc
                board.add((x,y))

            elif it2 == "S" :
                board.add((x,y))
                spike.add((x,y))

            x = x+1
        y = y-1

grid_to_laby()
myLabyHell["walls"] = walls
myLabyHell["spike"] = spike
myLabyHell["board"] = board
myLabyHell["final"] = final

#print(myLabyHell)




def vocabulary(board, t_max):
    directions = 'gdhb'
    do_vars = [('do', t, a) for t in range(t_max) for a in directions]
    at_vars = [('at', t, c) for t in range(t_max + 1) for c in board]
    return {v: i + 1 for i, v in enumerate(do_vars + at_vars)}


def clauses_exactly_one_action(var2n, t_max):
    directions = 'gdhb'
    at_least_one_action = [[var2n[('do', t, a)] for a in directions] for t in range(t_max)]
    at_most_one_action = [[-var2n[('do', t, a1)], -var2n[('do', t, a2)]]
                          for t in range(t_max) for a1, a2 in combinations(directions, 2)]
    return at_least_one_action + at_most_one_action


def clauses_initial_state(var2n, board, initial):
    cl = []
    for c in board:
        if c == initial:
            cl.append([var2n[('at', 0, c)]])
        else:
            cl.append([-var2n[('at', 0, c)]])
    return cl


def succ(at, direction, board):
    x, y = at
    return {'g': (x - 1, y), 'd': (x + 1, y), 'h': (x, y + 1), 'b': (x, y - 1)}[direction]


def clauses_successor_from_given_position(var2n, board, spike, t_max, position):
    directions = 'gdhb'
    Successors = {a: succ(position, a, board) for a in directions}
    # transitions impossibles, entre deux cases non voisines
    cl = [[-var2n[('at', t, position)], -var2n[('at', t + 1, c)]]
          for t in range(t_max) for c in board if not (c in Successors.values())]
    # actions interdites, qui feraient sortir du plateau (mur ou bord)
    cl += [[-var2n[('at', t, position)], -var2n[('do', t, a)]]
           for t in range(t_max) for a, c in Successors.items() if not (c in board)]
    # transitions possibles
    for a, c in Successors.items():
        if c in board:
            # at(t,position) AND do(t,a) -> at(t+1,c)
            cl += [[-var2n[('at', t, position)], -var2n[('do', t, a)], var2n[('at', t + 1, c)]]
                   for t in range(t_max)]
            # unicitÃ© de l'Ã©tat Ã  l'issue de l'action
            cl += [[-var2n[('at', t, position)], -var2n[('do', t, a)], -var2n[('at', t + 1, c1)]]
                   for t in range(t_max) for c1 in Successors.values() if c1 != c and c1 in board]
    return cl


def sat_laby1(board, spike, initial, final, t_max):
    var2n = vocabulary(board, t_max)
    clauses = clauses_exactly_one_action(var2n, t_max) + clauses_initial_state(var2n, board, initial)
    for c in board:
        clauses += clauses_successor_from_given_position(var2n, board, spike, t_max, c)
    tmp = []
    for f in final:
        if f in board:
            tmp.append(var2n[('at', t_max, f)])
    clauses.append(tmp)
    return var2n, clauses


def clauses_to_dimacs(clauses: List[List[int]], nb_vars: int) -> str:
    chaine = ""

    chaine += f"p cnf {nb_vars} {len(clauses)}\n"
    print(chaine)
    for clause in clauses:
        # print(f"clause: {clause}")
        if type(clause) is int:
            chaine += f"{clause}"
        else:
            for var in clause:
                chaine += f"{var}"
                if clause[len(clause)-1] != var:
                    chaine += " "
        chaine += " 0\n"
    return chaine

def write_dimacs_file(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)

def exec_gophersat(filename: str, cmd: str = "./gophersat", encoding: str = "utf8") -> Tuple[bool, List[int]]:
    result = subprocess.run(
        [cmd, filename], capture_output=True, check=True, encoding=encoding
    )
    string = str(result.stdout)
    lines = string.splitlines()

    if lines[1] != "s SATISFIABLE":
        return False, []

    model = lines[2][2:].split(" ")

    return True, [int(x) for x in model]

def solve_laby1(laby, t_max):
    #t = t_max
    for t in range(1, t_max):
        v2n, cl = sat_laby1(laby["board"],laby["spike"], laby["initial"], laby["final"], t)
        #print("state1 : ",v2n)
        n2v = {i: v for v, i in v2n.items()}
        #print("state2 : ",n2v)
        dimacs = clauses_to_dimacs(cl, len(v2n))
        filename = f'laby1_{t!s}.cnf'
        filename = ".cnf/"+filename
        write_dimacs_file(dimacs, filename)
        sat, model = exec_gophersat(filename)
        if sat:
            return [n2v[i] for i in model if i > 0 and n2v[i][0] == 'do']
        else:
            print('pas de plan de taille', t)

print(solve_laby1(myLabyHell, infosGrille["max_steps"]))