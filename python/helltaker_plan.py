import sys
from helltaker_utils import grid_from_file, check_plan
import numpy as np
from collections import namedtuple

#superclass representant les entites dans la map
class Entity:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    # retourne le tuple de la position
    def position(self):
        return self.x,self.y

#classe qui représente les trucs qui bougent dans le plan
class Fluent(Entity):
    def __init__(self,x,y):
        super().__init__(x,y)

    def move_up(self):
        self.y += 1
    def move_down(self):
        self.y -=1
    def move_right(self):
        self.x += 1
    def move_left(self):
        self.y -= 1


class Key(Entity):
    def __init__(self,x,y,isTaken=false):
        super().__init__(x,y)
        self.isTaken = isTaken

class Box(Fluent):
    def __init__(self,x,y):
        super().__init__(x,y)

class staticTrap(Entity):
    def __init__(self,x,y):
        super().__init__(x,y)

class dynamicTrap(staticTrap):
    def __init__(self,x,y,open = false):
        super().__init__(x,y)
        self.open = open

class Demoness(Entity):
    def __init__(self,x,y):
        super().init(x,y)

#objet qui represente le heros dans la map, par defaut n'as pas de clef
class Hero(Fluent):
    def __init__(self,x,y,key,stepsLeft):
        super().__init__(x,y)
        self.key = key
        self.stepsLeft = stepsLeft #nombre de moves qui reste au personnage

    def hasKey(self):
        return self.key.isTaken

class MazeReader:
    def __init__(self,infos): #avec infos = grid_from_file(filename)
        self.grid = infos["grid"]
        self.title = infos["title"]
        self.maxSteps = infos["max_steps"]
        self.lines = infos["m"]
        self.columns = infos["n"]

class Maze:
    def __init__(self,hero,boxes,staticTraps,dynamicTraps,demoness,):

Action = namedtuple('action',('verb','direction'))

actionType = ["move","push","moveBlock","pushMob","killMob","Unlock"]




#"hbgd" -> haut bas gauche droite
def monsuperplanificateur(infos):
    return "hbgd"


def main():
    # récupération du nom du fichier depuis la ligne de commande
    filename = sys.argv[1]

    # récupération de al grille et de toutes les infos
    infos = grid_from_file(filename)

    # calcul du plan
    plan = monsuperplanificateur(infos)

    # affichage du résultat
    if check_plan(plan):
        print("[OK]", plan)
    else:
        print("[Err]", plan, file=sys.stderr)
        sys.exit(2)


if __name__ == "__main__":
    main()