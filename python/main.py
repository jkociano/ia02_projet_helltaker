import sys

# from helltaker_utils import *
from collections import namedtuple
from typing import Dict, List, Tuple, Callable, Set

from helltaker_utils import grid_from_file, check_plan
import time

# NOUS AVONS REUTILISER BEAUCOUP DE FONCTION UTILITAIRE DU TP SOKOBAN

# Representation actions
Action = namedtuple("action", ("verb", "direction"))
actionNames = ["move", "push", "kill", "unlock/cle"]

actions = {d: [] for d in "hbgd"}

for d in "hbgd":
    for a in actionNames:
        actions[d].append(Action(a, d))

State = namedtuple(
    "state",
    ("heros", "steps", "blocks", "cle", "lock", "mobs", "safePiege", "unsafePiege"),
)

# Utilitaires


def add_in_frozenset(fset: frozenset, elt: Tuple[int, int]) -> frozenset:
    s = {x for x in fset}
    s.add(elt)
    return frozenset(s)


def remove_in_frozenset(fset: frozenset, elt: Tuple[int, int]) -> frozenset:
    s = {x for x in fset}
    s.remove(elt)
    return frozenset(s)


# dictionnaire vers chemin
def dict2path(s: State, d: Dict[State, Tuple[State, Action]]) -> List[str]:
    l = [(s, None)]
    while not d[s] is None:
        parent, a = d[s]
        l.append((parent, a.direction))
        s = parent
    l.reverse()
    return l


# Recuperation des elements sur la map prédicats d'une part fluent de l'autre
def lectureInfos(grid: List[List[str]], maxstep: int) -> Tuple[Dict[str, set], State]:
    heros = None
    blocks = set()
    cle = None
    lock = None
    mobs = set()
    safePiege = set()
    unsafePiege = set()

    map_rules = {"D": set(), "S": set(), "#": set()}

    for x, subgrid in enumerate(grid):
        for y, c in enumerate(subgrid):
            if c in "DS#O":
                if c == "O":
                    c = "S"
                map_rules[c].add((x, y))
            elif c == "H":
                heros = (x, y)
            elif c in ["B", "P", "Q", "O"]:
                blocks.add((x, y))
            elif c in ["K"]:
                cle = (x, y)
            elif c in ["L"]:
                lock = (x, y)
            elif c in ["M"]:
                mobs.add((x, y))
            elif c in ["T", "P"]:
                safePiege.add((x, y))
            elif c in ["U", "Q"]:
                unsafePiege.add((x, y))

    state = State(
        heros,
        maxstep,
        frozenset(blocks),
        cle,
        lock,
        frozenset(mobs),
        frozenset(safePiege),
        frozenset(unsafePiege),
    )

    return map_rules, state


# Pour inserer/retirer de la pile


def insert_tail(s, l):
    l.append(s)
    return l


def remove_head(l):
    return l.pop(0), l


def remove_tail(l):
    return l.pop(), l


# bouger d'une case
def one_step(position: Tuple[int, int], direction: str) -> Tuple[int, int]:
    x, y = position
    return {"d": (x, y + 1), "g": (x, y - 1), "h": (x - 1, y), "b": (x + 1, y)}[
        direction
    ]


# modifier l'etat
def modify_state_factory(state: State) -> Callable:
    def mod(
        heros=state.heros,
        steps=state.steps,
        blocks=state.blocks,
        cle=state.cle,
        lock=state.lock,
        mobs=state.mobs,
        safePiege=state.safePiege,
        unsafePiege=state.unsafePiege,
    ):
        return State(heros, steps, blocks, cle, lock, mobs, safePiege, unsafePiege)

    return mod


def moving_frozenset(
    fset: frozenset, supr: Tuple[int, int], nouveau: Tuple[int, int] = None
):
    raw = {x for x in fset}
    raw.remove(supr)
    raw.add(nouveau)
    return frozenset(raw)


# retourne les cases libres
def free_factory(map_rules):
    def free(position):
        return not ((position in map_rules["#"]) or (position in map_rules["D"]))

    return free


def maj_state(
    map_rules: Dict[str, List[int]],
    state: State,
    nouveauheros: List[int],
    mobs: List[Tuple[int, int]] = [],
    blocks: List[Tuple[int, int]] = [],
    cleFinded: bool = False,
    lockOpenable: bool = False,
):

    # Tuer mob
    nouveauMobs = []
    if mobs:
        nouveauMobs = list(mobs)
    else:
        nouveauMobs = list(state.mobs)
    for mob in nouveauMobs:
        if (mob in state.safePiege) or (mob in map_rules["S"]):
            nouveauMobs.remove(mob)

    # bouge un block
    nouveauBlocks = []
    if blocks:
        # print(blocks)
        nouveauBlocks = list(blocks)
    else:
        nouveauBlocks = state.blocks

    # Indique le cout
    cout = 0
    if (nouveauheros in state.safePiege) or (nouveauheros in map_rules["S"]):
        cout = 2
    else:
        cout = 1

    # cle et coffre PROBLEME ICI PROBABLEMENT PUISQUE PERSO GERE MAL LES PORTES
    nouveauLock = state.lock
    if lockOpenable:
        nouveauLock = tuple()
    nouveaucle = state.cle
    if cleFinded:
        nouveaucle = tuple()

    # si trop de pas
    if state.steps - cout < 0:
        return None

    # modifie les pics, les monstres et boites
    else:
        return modify_state_factory(state)(
            heros=nouveauheros,
            safePiege=state.unsafePiege,
            unsafePiege=state.safePiege,
            mobs=frozenset(nouveauMobs),
            steps=state.steps - cout,
            blocks=frozenset(nouveauBlocks),
            lock=nouveauLock,
            cle=nouveaucle,
        )


def do_inplace(action: Action, state: State, map_rules: Dict[str, set]):

    heros = state.heros
    goal = map_rules["D"]
    blocks = state.blocks
    mobs = state.mobs
    spikes = map_rules["S"]
    safePiege = state.safePiege
    unsafePiege = state.unsafePiege
    cle = state.cle
    lock = state.lock

    nouveauheros = one_step(heros, action.direction)

    # DEFINES FACTORIES
    free = free_factory(map_rules)

    if action.verb == "move":
        if free(nouveauheros) and not (nouveauheros in mobs | blocks):
            return maj_state(map_rules, state, nouveauheros)

    if action.verb == "push":
        nouveauPose = one_step(nouveauheros, action.direction)

        if (
            (nouveauheros in blocks | mobs)
            and free(nouveauPose)
            and not (nouveauPose in mobs | blocks)
            and not nouveauPose == lock
        ):
            if nouveauheros in blocks:
                return maj_state(
                    map_rules,
                    state,
                    heros,
                    blocks=moving_frozenset(blocks, nouveauheros, nouveauPose),
                )
            if nouveauheros in mobs and nouveauPose != cle:
                return maj_state(
                    map_rules,
                    state,
                    heros,
                    mobs=moving_frozenset(mobs, nouveauheros, nouveauPose),
                )

    if action.verb == "kill":
        nouveauPose = one_step(nouveauheros, action.direction)
        if (nouveauheros in mobs) and (
            not free(nouveauPose) or nouveauPose in blocks or nouveauPose == lock
        ):
            if not moving_frozenset(mobs, nouveauheros, nouveauPose):
                return maj_state(
                    map_rules,
                    state,
                    heros,
                    mobs=moving_frozenset(mobs, nouveauheros),
                    mobCleared=True,
                )
            else:
                return maj_state(
                    map_rules, state, heros, mobs=moving_frozenset(mobs, nouveauheros)
                )

    return None


# decrit le but cad case à coté de demon
def goal_factory(map_rules: Dict[str, set]) -> Callable[[State], bool]:
    def goals(state: State):
        offsets = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        goalsCases = [(state.heros[0] + x[0], state.heros[1] + x[1]) for x in offsets]
        for demon in map_rules["D"]:
            if demon in goalsCases:
                return True
        return False

    return goals


# retourne successeurs
def succ_factory(map_rules: Dict[str, set]) -> Set[Tuple[State, Action]]:
    def succ(state):
        l = []
        for x in actions.values():
            for a in x:
                l.append((do_inplace(a, state, map_rules), a))
        return {el[0]: el[1] for el in l}

    return succ


# algo de recherche
def search_with_parent(s0, goals, succ, remove, insert):
    l = [s0]
    save = {s0: None}
    s = s0
    while l:
        s, l = remove(l)
        for s2, a in succ(s).items():
            if not s2:
                continue
            if not s2 in save:
                save[s2] = (s, a)
                if goals(s2):
                    return s2, save
                insert(s2, l)
    return None, save


def main():
    print("main")
    # récupération du nom du fichier depuis la ligne de commande
    start_time = time.time()
    filename = sys.argv[1]

    # récupération de al grille et de toutes les infos
    infos = grid_from_file(filename)
    map_rules, s0 = lectureInfos(infos["grid"], infos["max_steps"])
    end, save = search_with_parent(
        s0, goal_factory(map_rules), succ_factory(map_rules), remove_head, insert_tail
    )

    # calcul du plan
    if end:
        plan = "".join([a for _, a in dict2path(end, save) if a])
        if check_plan(plan):
            print("[OK]", plan)
        else:
            print("[Err]", plan, file=sys.stderr)
    else:
        print("Pas de solution trouvée")
    print("--- %s seconds ---" % (time.time() - start_time))
    sys.exit(2)


if __name__ == "__main__":
    main()
