# IA02_projet_Helltaker

Dans ce projet on résout le helltaker. Fin on essaie, on y arrive pas tout à fait mais on essaie. 

## Structure de notre repo et lancement de fichiers

### 1. Python
Pour lancer la recherche en python on écrit dans notre terminal : 

python python/main.py monLevel.txt  

On change le level en fonction du level qu'on veut résoudre. 

Nous avons intentionnelemt laissé le fichier helltaker_plan.py dans lequel nous nous sommes penché sur une répresentation du probmlème et sa résolution en programmation orientée objet. 

### 2. ASP 
Nous n'avons pas implémenté le génerateur d'ASP en Python, pour lancer les fichiers ASP la commande clingo qui est au début du chaque fichier : 
clingo -c horizon=32 -n0 nv3.lp

### 3. SAT
Le dossier SAT est composé du fichier SAT.py qui s'occupe de générer un fichier dimacs, l'exécuter, afficher le résultat.
SAT.py prend en paramètres corridor.txt ou simpleSpikes.txt (inclus au dossier).
Sur linux on peut l'appeler de la sorte : python3 SAT.py corridor.txt
